/*
 * Command Monitor for Arduino
 * 
 * Copyright Jean-Luc Béchennec 2015
 *
 * This software is distributed under the GNU Public Licence v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CommandMonitor_h__
#define __CommandMonitor_h__

#include "Arduino.h"
#include "HardwareSerial.h"

class CommandMonitorCommand;
class CommandMonitor;

typedef void (*commandFunction)(CommandMonitor &monitor);

class CommandMonitor
{
private:
  CommandMonitorCommand *mCommandList;
  HardwareSerial &mSerialLink;
  CommandMonitorCommand *mCommandInProgress;
  
public:
  CommandMonitor(HardwareSerial &serialLink) :
    mCommandList(NULL), mSerialLink(serialLink), mCommandInProgress(NULL) {}
  void addCommand(byte commandCode, commandFunction func);
  void addByteArg();
  void addUIntArg();
  void addULongArg();
  void write(byte reply) { mSerialLink.write(reply); }
  void write(unsigned int reply);
  void write(unsigned long reply);
  byte getByteArg();
  unsigned int getUIntArg();
  unsigned long getULongArg();
  void update();
};


class CommandMonitorCommand
{
private:
  byte mCommand;
  byte mArgSize;
  commandFunction mFunction;
  CommandMonitorCommand *mNext;
  CommandMonitor &mMonitor;
  
public:
  CommandMonitorCommand(
    byte code,
    commandFunction func,
    CommandMonitorCommand* next,
    CommandMonitor& monitor) :
    mCommand(code), mArgSize(0), mFunction(func),
    mNext(next), mMonitor(monitor) {}
  void addByteArg()    { mArgSize++;    }
  void addUIntArg()    { mArgSize += 2; }
  void addULongArg()   { mArgSize += 4; }
  void execute()       { mFunction(mMonitor); }
  byte argSize()       { return mArgSize; }
  byte commandCode()   { return mCommand; }
  CommandMonitorCommand *nextCommand()   { return mNext; }
};

#endif
