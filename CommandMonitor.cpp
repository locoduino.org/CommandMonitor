/*
 * Command Monitor for Arduino
 * 
 * Copyright Jean-Luc Béchennec 2015
 *
 * This software is distributed under the GNU Public Licence v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
#include "CommandMonitor.h"

static const byte ACK = 0;
static const byte ERR = 0xFF;

void CommandMonitor::addCommand(byte commandCode, commandFunction func)
{
  mCommandList = new CommandMonitorCommand(
                       commandCode,
                       func,
                       mCommandList,
                       *this);
}

void CommandMonitor::addByteArg()
{
  if (mCommandList) mCommandList->addByteArg();
}

void CommandMonitor::addUIntArg()
{
  if (mCommandList) mCommandList->addUIntArg();
}

void CommandMonitor::addULongArg()
{
  if (mCommandList) mCommandList->addULongArg();
}

void CommandMonitor::update()
{
  if (mCommandInProgress) {
    /* a command is in progress, wait until the buffer has all its args */
    if (mSerialLink.available() >= mCommandInProgress->argSize()) {
      /* acknowledge */
      mSerialLink.write(ACK);
      /* arguments available, execute the command */
      mCommandInProgress->execute();
      /* command no longer in progress */
      mCommandInProgress = NULL;
    }
  }
  else if (mSerialLink.available() > 0) {
    /* Get the command code */
    byte command = mSerialLink.read();
    /* lookup the command */
    CommandMonitorCommand *currentCommand = mCommandList;
    while (currentCommand)
    {
      if (currentCommand->commandCode() == command) {
        mCommandInProgress = currentCommand;
        break;
      }
      currentCommand = currentCommand->nextCommand();
    }
    if (! mCommandInProgress) {
      /* command not found, return the error code */
      mSerialLink.write(ERR);
    }
  }
}

byte CommandMonitor::getByteArg()
{
  return mSerialLink.read();
}

unsigned int CommandMonitor::getUIntArg()
{
  return mSerialLink.read() << 8 | mSerialLink.read();
}

unsigned long CommandMonitor::getULongArg()
{
  return mSerialLink.read() << 24 |
         mSerialLink.read() << 16 |
         mSerialLink.read() << 8 |
         mSerialLink.read();
}

void CommandMonitor::write(unsigned int reply)
{
  mSerialLink.write(reply >> 8);
  mSerialLink.write(reply & 0xFF);  
}

void CommandMonitor::write(unsigned long reply)
{
  mSerialLink.write(reply >> 24);
  mSerialLink.write((reply >> 16) & 0xFF);  
  mSerialLink.write((reply >> 8) & 0xFF);  
  mSerialLink.write(reply & 0xFF);  
}
